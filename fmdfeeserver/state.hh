//__________________________________________________________________
struct State : public RcuCE::Service 
{
  State(const std::string s, int& state) 
      : _serv(s), _state(state) 
  { _service = &_serv; }
  int Update() {
    _serv = _state;
    return 0;
  }
  FeeServer::IntService _serv;
  int& _state;
};
