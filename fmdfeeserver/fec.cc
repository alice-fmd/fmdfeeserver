#include "fec.hh"
#include <feeserverxx/fee_servt.hh>
#include <feeserverxx/fee_msg.hh>
#include <feeserverxx/fee_main.hh>
#include <iostream>
#include <cerrno>
#include <cstring>
#include <iomanip>

//__________________________________________________________________
bool Fec::_also_thresholds  = true;
bool Fec::_glitch_filtering = false;

//__________________________________________________________________
unsigned int Fec::_delay = 0;

//__________________________________________________________________
Fec::Fec(unsigned short addr, FeeServer::Main& m, RcuCE::Rcu& r) 
  : RcuCE::Fec(addr, m, r), 
    _csr0(0),
    _csr1(0),
    _t1(0),
    _flash_i(0),
    _al_dig_i(0),
    _al_ana_i(0),
    _va_rec_ip(0),
    _t2(0),
    _va_sup_ip(0),
    _va_rec_im(0),
    _va_sup_im(0),
    _gtl_u(0),
    _t3(0),
    _t1_sens(0),
    _t2_sens(0),
    _al_dig_u(0),
    _al_ana_u(0),
    _t4(0),
    _va_rec_up(0),
    _va_sup_up(0),
    _va_sup_um(0),
    _va_rec_um(0),
    _t1_th(0),
    _flash_i_th(0),
    _al_dig_i_th(0),
    _al_ana_i_th(0),
    _va_rec_ip_th(0),
    _t2_th(0),
    _va_sup_ip_th(0),
    _va_rec_im_th(0),
    _va_sup_im_th(0),
    _gtl_u_th(0),
    _t3_th(0),
    _t1_sens_th(0),
    _t2_sens_th(0),
    _al_dig_u_th(0),
    _al_ana_u_th(0),
    _t4_th(0),
    _va_rec_up_th(0),
    _va_sup_up_th(0),
    _va_sup_um_th(0),
    _va_rec_um_th(0),
    _t_ctor(0)
{
  _t_ctor    = time(NULL);
  _csr0      = AddService<int,FeeServer::IntTrait>("CSR0",      0x11);
  _csr1      = AddService<int,FeeServer::IntTrait>("CSR1",      0x12);
  _t1        = AddService<int,FeeServer::IntTrait>("T1",        0x6);
  _flash_i   = AddService<int,FeeServer::IntTrait>("FLASH_I",   0x7);//3.3
  _al_dig_i  = AddService<int,FeeServer::IntTrait>("AL_DIG_I",  0x8);//2.5
  _al_ana_i  = AddService<int,FeeServer::IntTrait>("AL_ANA_I",  0x9);//2.5
  _va_rec_ip = AddService<int,FeeServer::IntTrait>("VA_REC_IP", 0xA);//2.5
  _t2        = AddService<int,FeeServer::IntTrait>("T2",        0x32);
  _va_sup_ip = AddService<int,FeeServer::IntTrait>("VA_SUP_IP", 0x33);//1.5
  _va_rec_im = AddService<int,FeeServer::IntTrait>("VA_REC_IM", 0x34);//-2
  _va_sup_im = AddService<int,FeeServer::IntTrait>("VA_SUP_IM", 0x35);//-2
  _gtl_u     = AddService<int,FeeServer::IntTrait>("GTL_U",     0x36);//2.5
  _t3        = AddService<int,FeeServer::IntTrait>("T3",        0x3C);
  _t1_sens   = AddService<int,FeeServer::IntTrait>("T1_SENS",   0x3D);//
  _t2_sens   = AddService<int,FeeServer::IntTrait>("T2_SENS",   0x3E);//
  _al_dig_u  = AddService<int,FeeServer::IntTrait>("AL_DIG_U",  0x3F);//2.5
  _al_ana_u  = AddService<int,FeeServer::IntTrait>("AL_ANA_U",  0x40);//2.5
  _t4        = AddService<int,FeeServer::IntTrait>("T4",        0x46);
  _va_rec_up = AddService<int,FeeServer::IntTrait>("VA_REC_UP", 0x47);//2.5
  _va_sup_up = AddService<int,FeeServer::IntTrait>("VA_SUP_UP", 0x48);//1.5
  _va_sup_um = AddService<int,FeeServer::IntTrait>("VA_SUP_UM", 0x49);//-2
  _va_rec_um = AddService<int,FeeServer::IntTrait>("VA_REC_UM", 0x4A);//-2

  if (!_also_thresholds) return;
  _t1_th        = AddService<int,FeeServer::IntTrait>("T1_TH",        0x01);
  _flash_i_th   = AddService<int,FeeServer::IntTrait>("FLASH_I_TH",   0x02);
  _al_dig_i_th  = AddService<int,FeeServer::IntTrait>("AL_DIG_I_TH",  0x03);
  _al_ana_i_th  = AddService<int,FeeServer::IntTrait>("AL_ANA_I_TH",  0x04);
  _va_rec_ip_th = AddService<int,FeeServer::IntTrait>("VA_REC_IP_TH", 0x05);
  _t2_th        = AddService<int,FeeServer::IntTrait>("T2_TH",        0x2D);
  _va_sup_ip_th = AddService<int,FeeServer::IntTrait>("VA_SUP_IP_TH", 0x2E);
  _va_rec_im_th = AddService<int,FeeServer::IntTrait>("VA_REC_IM_TH", 0x2F);
  _va_sup_im_th = AddService<int,FeeServer::IntTrait>("VA_SUP_IM_TH", 0x30);
  _gtl_u_th     = AddService<int,FeeServer::IntTrait>("GTL_U_TH",     0x31);
  _t3_th        = AddService<int,FeeServer::IntTrait>("T3_TH",        0x37);
  _t1_sens_th   = AddService<int,FeeServer::IntTrait>("T1_SENS_TH",   0x38);
  _t2_sens_th   = AddService<int,FeeServer::IntTrait>("T2_SENS_TH",   0x39);
  _al_dig_u_th  = AddService<int,FeeServer::IntTrait>("AL_DIG_U_TH",  0x3A);
  _al_ana_u_th  = AddService<int,FeeServer::IntTrait>("AL_ANA_U_TH",  0x3B);
  _t4_th        = AddService<int,FeeServer::IntTrait>("T4_TH",        0x41);
  _va_rec_up_th = AddService<int,FeeServer::IntTrait>("VA_REC_UP_TH", 0x42);
  _va_sup_up_th = AddService<int,FeeServer::IntTrait>("VA_SUP_UP_TH", 0x43);
  _va_sup_um_th = AddService<int,FeeServer::IntTrait>("VA_SUP_UM_TH", 0x44);
  _va_rec_um_th = AddService<int,FeeServer::IntTrait>("VA_REC_UM_TH", 0x45);
  // AddStatusService();
}
//__________________________________________________________________
Fec::~Fec() 
{
  INFO(_main, ("Removing FEC 0%02x", int(_address)));
  std::cout << "Removing FEC " << int(_address) << std::endl;
}
//__________________________________________________________________
int
Fec::UpdateAll()
{
  unsigned int now = time(NULL);
  if (now - _t_ctor < _delay) {
    std::cout << "T since construction: " << now << " - " << _t_ctor 
	      << " = " << now - _t_ctor << " is smaller than " 
	      << _delay;
    return 0;
  }
  int ret  = RcuCE::Fec::UpdateAll();
  int cret = CheckFaults(ret);
  return cret;
}

//____________________________________________________________________
FecFactory::FecFactory(FeeServer::Main& m, RcuCE::Rcu& r) 
  : RcuCE::FecFactory(m,r)
{}
RcuCE::Fec* 
FecFactory::MakeFec(unsigned char addr) 
{
  INFO(_main, ("Making FEC 0x%02x", int(addr)));
  std::cout << "Making FEC 0x" << std::setfill('0') 
	    << std::hex << int(addr) << std::setfill(' ') 
	    << std::dec << std::endl;
  return new Fec(addr, _main, _rcu);
}

//____________________________________________________________________
const std::string&
FecFactory::Version()
{
  static std::string version = PACKAGE_VERSION;
  return version;
}

//____________________________________________________________________
const std::string&
FecFactory::Package()
{
  static std::string package = PACKAGE_NAME;
  return package;
}

//__________________________________________________________________
//
// EOF
//
