#!/bin/sh

###############################################################################
#                                                                             #
# This script is part of the FeeCommunication Software for the DCS (TPC, TRD) #
# of the ALICE experiment at CERN. It starts the FeeServer and takes care of  #
# updating and restarting commands. It ends only , when the FeeServer is      #
# terminated (which way soever) and no updateFeeServer or restartFeeServer    #
# command has triggered the termination.                                      #
#                                                                             #
# @author: S. Bablok, C. Kofler (ZTT FH Worms)                                #
# (for more information: http://www.ztt.fh-worms.de)                          #
#                                                                             #
###############################################################################

# this is nice but not applicable for the DCS board, since the mounted
# network disk might have different paths on the build machine and the board
# prefix=@prefix@
# exec_prefix=@exec_prefix@
# bindir=@bindir@
# sbindir=@sbindir@
bindir=`dirname $0`
server_name=@PACKAGE@
Server_Name="@PACKAGE_NAME@"

echo " "
echo "starting FeeServer $FEE_SERVER_NAME"
echo "  Have fun ;-)"
date

doRestart=1

# 
# Loop forever to automatically respawn
#
while [ $doRestart -ne 0 ] ; do
    $bindir/${server_name} $@
    retVal=$?

    while [ $retVal -eq 3 ] || [ $retVal -eq 2 ] ; do
        #echo "Exit status of FeeServer: $retVal"

	sleep 1
	echo "killing all feeserver processes to prepare restart"
	killall -q -KILL ${server_name}
	echo " "

	# prepare update only if "newFeeServer" exists and is non-zero
	if [ $retVal -eq 3 ] && [ -s new${server_name} ]
	then
	    echo "preparing new binary for update"
	    mv ${server_name} ${server_name}.org
	    mv new${server_name} ${server_name}
	    chmod 777 ${server_name}
	    echo "${Server_Name} update finalised"
	fi

	echo " "
	if [ $IP_TIMESERVER ] ; then
	    echo "updating time on board"
	    # retrieving current date and time
	    rdate $IP_TIMESERVER
	fi

	echo "restarting ${Server_Name}..."
	echo " "
	$bindir/${server_name} $@
	retVal=$?

    done
    echo "${Server_Name} exited with status $retVal."

    if [ $retVal -eq 0 ] ; then
	echo "Clean exit"
	doRestart=0
    elif [ $retVal -eq 201 ] ; then
	echo "Insufficient memory!"
	doRestart=0
    elif [ $retVal -eq 202 ] ; then
	echo "No ${Server_Name} name specified!"
	doRestart=0
    elif [ $retVal -eq 203 ] ; then
	echo "No DIM_DNS_NODE specified!"
	doRestart=0
    elif [ $retVal -eq 204 ] ; then
	echo "DIM framework exited ${Server_Name}"
	echo "(maybe Server with same name already exists)."
	doRestart=0
    elif [ $retVal -eq 205 ] ; then
	echo "Unable to run as DIM Server"
	doRestart=0
    else
	echo "${Server_Name} exited due to unknown error!"
	echo "Trying to restart ${Server_Name} now ..."
	echo ""
    fi
done

echo " -- BYE --"
echo " "
#
# EOF
#

