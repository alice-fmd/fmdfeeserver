#include <dim/dic.hxx>
#include "option.hh"

//____________________________________________________________________
const char*
serviceName(const std::string& server, const char* what)
{
  static std::string buffer;
  std::stringstream s;
  s << server << "_" << what;
  buffer = s.str();
  return buffer.c_str();
}

  
//____________________________________________________________________
struct RcuState : public DimStampedInfo
{
  RcuState(const std::string& server, int freq) 
    : DimStampedInfo(serviceName(server, "RCU_STATE"), freq, (void*)0, 0)
  {
  }
  void infoHandler()
  {
    time_t      now   = getTimestamp();
    void*       data  = getData();
    size_t      size  = getSize();
    struct tm*  ltime = localtime(&now);
    static char tbuf[256];
    strftime(tbuf, 256, "%d %b %Y %T", ltime);

    std::cout << getName() << " [" << tbuf << "]: ";

    if (data == 0 || size == 0) { 
      std::cout << " has no link" << std::endl;
      return;
    }
    int value = getInt();
    std::cout << value << std::endl;
  }
};

int main(int argc, char** argv) 
{
  command_line cl(argv[0]);
  option<std::string>  opt_n('n', "Set name of server", "FMD-FEE_0_0_0","NAME");
  option<std::string>  opt_N('N', "Set DIM DNS host", "localhost", "HOST");
  option<unsigned int> opt_f('f', "Update frequency", 0, "SECS");
  cl.add_option(opt_n);
  cl.add_option(opt_N);
  cl.add_option(opt_f);
  if (!cl.handle(argc, argv)) return 1;
  if (cl.got_help()) return 0;
  
  std::string  name     = opt_n;
  std::string  dns      = opt_N;
  int          freq     = opt_f;

  DimClient::setDnsNode(const_cast<char*>(dns.c_str()));
  
  RcuState rcuState(name, freq);
  
  while (true) { 
    sleep(5);
  }
  
  return 0;
}

  
    
  
