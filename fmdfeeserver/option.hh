// -*- mode: c++ -*-
/**
 * @file   option.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sat Jul 12 23:44:15 2008
 * 
 * @brief  Command line option parsing
 * 
 * 
 */
#ifndef OPTION_HH
# define OPTION_HH
# include <string>
# include <sstream>
# include <map>
# include <iomanip>
# include <iostream>

/**
 * @defgroup options Command line option parsing 
 */
//____________________________________________________________________  
/** 
 * Function to turn a string into a value.
 * 
 * @param str String to convert
 * @param v   Converted value
 * 
 * @return The converted value 
 * @ingroup options
 */
template <typename T>
T&
str2val(const char* str, T& v)
{
  std::stringstream s(str);
  s >> v;
}

//____________________________________________________________________  
/**
 * Base class for options
 * 
 * @ingroup options
 */
struct option_base 
{
  /** 
   * Constructor
   * 
   * @param opt Short option
   * @param help  Help string.
   * @param arg Argument string (if empty, then no argument is
   *            needed).
   */  
  option_base(char opt, const char* help, const char* arg="") 
    : _opt(opt), _help(help), _arg(arg)
  {}
  /** 
   * Destructor
   */  
  virtual ~option_base() {}
  /** 
   * Print help string to passed std::ostream object. 
   * 
   * @param out Output stream to print help string to.  Normally this
   *            will be something like std::cout.
   */  
  virtual void print_help(std::ostream& out) const
  { 
    out << "\t-" << _opt << " " << std::left << std::setw(10) << _arg 
	<< " " << std::setw(40) << _help;
  }
  /** 
   * Whether we can handle the passed option.
   * @return Should return @c true if the object can handle the
   *         option. 
   */  
  virtual bool handle() = 0;
  /** 
   * Check if we can handle the passed argument.
   * 
   * @param arg Argument to check for.
   * 
   * @return Should return @c true if the object can handle the
   *         option.
   */
  virtual bool handle(const char* arg) = 0;
  /** 
   * Whether this option needs an argument or not.
   * 
   * @return Returns false if argument @a arg to constructor is
   *         empty. 
   */
  virtual bool need_arg() const { return !_arg.empty(); }
  /** 
   * Get the option character. 
   * 
   * @return The option character.
   */
  virtual char opt() const { return _opt; }
protected:
  /**
   * Option character
   */
  char _opt;
  /** Help string */
  std::string _help;
  /** Argument string - if any */
  std::string _arg;
};

//____________________________________________________________________  
/**
 * Class template for options
 * @ingroup options
 */
template <typename T>
struct option : public option_base
{
  /** 
   * Constructor
   * 
   * @param opt Short option
   * @param help  Help string.
   * @param def  Default value
   * @param arg Argument string (if empty, then no argument is
   *            needed).
   */  
  option(char opt, const char* help, T def=T(), const char* arg="")
    : option_base(opt, help, arg), _value(def)
  {}
  /** 
   * Conversion operator.
   * 
   * @return Option value
   */  
  operator T() { return _value; }
  /** 
   * Conversion operator.
   * 
   * @return Option value
   */  
  operator const T() const { return _value; }
  /** 
   * Print help string to passed std::ostream object. 
   * 
   * @param out Output stream to print help string to.  Normally this
   *            will be something like std::cout.
   */  
  virtual void print_help(std::ostream& out) const { 
    option_base::print_help(out);
    out << "[" << _value << "]" << std::endl;
  }
  /** 
   * Whether we can handle the passed option.
   * @return Should return @c true if the object can handle the
   *         option. 
   */  
  virtual bool handle() { return toggle(_value); }
  /** 
   * Check if we can handle the passed argument.
   * 
   * @param arg Argument to check for.
   * 
   * @return Should return @c true if the object can handle the
   *         option.
   */
  virtual bool handle(const char* arg) { 
    if (_arg.empty()) return false; 
    if (!arg)         return false;
    std::stringstream s(arg);
    s >> _value;
    return true;
  }
  /** 
   * Toggle value (if that makes sense)
   * 
   * @param v Value to toggle.  On return, it has the "toggled"
   *          value. 
   * 
   * @return 
   */  
  bool toggle(T& v) { return false; } 
protected:
  /** Option value */
  T _value;
};

//____________________________________________________________________  
/** 
 * Specialisation of value toggle for boolean options (flags)
 * 
 * @param v Value to toggle.
 * 
 * @return Toggled value.
 */
template <>
bool 
option<bool>::toggle(bool& v) { v = !v; return true; }
//____________________________________________________________________  
/** 
 * Specialisation of handling member function for unsigned integers.
 * This will handle octa- and hexidecimal strings correctly.
 * 
 * @param arg String argument
 * 
 * @return @c true on success.
 */
template <>
bool 
option<unsigned int>::handle(const char* arg) 
{ 
  std::stringstream s(arg);
  if (arg[0] == '0') { 
    if (arg[1] == 'x' || arg[1] == 'X') s << std::hex;
    else                                s << std::oct;
  }
  s >> _value;
  return true; 
}
//____________________________________________________________________  
/**
 * Command line opion manager
 * @ingroup options
 */
struct command_line
{
  /** 
   * Constructor
   * 
   * @param name Name of application.
   */  
  command_line(const char* name) 
    : _options(), 
      _name(name),
      _help_opt('h', "Show this help", false)
  {
    add_option(_help_opt);
  }
  /**
   * Destructor 
   */
  ~command_line() {}
  /** 
   * Show usage information.
   * 
   */  
  void print_help() { 
    std::cout << "Usage: " << _name << " [OPTIONS]\n\n"
	      << "Options:" << std::endl;
    for (option_map::const_iterator i = _options.begin(); 
	 i != _options.end(); ++i) 
      i->second->print_help(std::cout);
  }
  /** 
   * Add an option 
   * 
   * @param o Option to add
   */  
  void add_option(option_base& o) { 
    char opt = o.opt();
    if (find_option(opt)) { 
      std::cerr << "Option " << opt << " already defined" << std::endl;
      return;
    }
    _options[opt] = &o;
  }
  /** 
   * Find an option.
   * 
   * @param o Option character to look for.
   * 
   * @return If the option is found, a pointer to the option,
   * otherwise 0
   */  
  option_base* find_option(char o) const { 
    option_map::const_iterator i = _options.find(o);
    if (i == _options.end()) return 0;
    return i->second;
  }
  /** 
   * Handle the command line
   * 
   * @param argc Number of arguments
   * @param argv Vector of arguments.
   * 
   * @return @c true on success, @c false otherwise
   */  
  bool handle(int argc, char** argv) { 
    for (int i = 1; i < argc; i++) { 
      if (argv[i][0] == '-') { 
	char o = argv[i][1];
	option_base* opt = find_option(o);
	if (!opt) { 
	  std::cerr << _name << ": unknown option '" << argv[i] << "'" 
		    << std::endl;
	  return false;
	}
	if (opt->need_arg()) {
	  if (!opt->handle(argv[++i])) return false;
	}
	else { 
	  if (!opt->handle()) return false;
	}
      }
    }
    if (_help_opt) print_help();
    return true;
  }
  /** 
   * Check if the help option was seen.
   * 
   * @return @c true of the help option was seen.
   */  
  bool got_help() const { return _help_opt; }
protected:
  /** Type of option map */
  typedef std::map<char,option_base*> option_map;
  /** Option map */
  option_map _options;
  /** Name of application */
  std::string _name;
  /** Help option. */
  option<bool> _help_opt;
};
  
    
     
#endif
//
// EOF
// 
