#ifndef FEC_HH
#define FEC_HH
#include <rcuce/rcuce_fec.hh>

struct Fec : public RcuCE::Fec 
{
  Fec(unsigned short addr, FeeServer::Main& m, RcuCE::Rcu& r);
  virtual ~Fec();
  virtual int UpdateAll();

  /** Whether to also publish thresholds */
  static bool _also_thresholds;
  /** How long to wait (in seconds) before updating for the first time */
  static unsigned int _delay;
  /** Whether to enable glitch filtering */
  static bool _glitch_filtering;
protected:
  RcuCE::Service* _csr0;
  RcuCE::Service* _csr1;
  RcuCE::Service* _t1;		// T1
  RcuCE::Service* _flash_i;	// FLASH_I
  RcuCE::Service* _al_dig_i;	// AL_DIG_I
  RcuCE::Service* _al_ana_i;	// AL_ANA_I
  RcuCE::Service* _va_rec_ip;	// VA_REC_IP
  RcuCE::Service* _t2;		// T2
  RcuCE::Service* _va_sup_ip;	// VA_SUP_IP
  RcuCE::Service* _va_rec_im;	// VA_REC_IM
  RcuCE::Service* _va_sup_im;	// VA_SUP_IM
  RcuCE::Service* _gtl_u;	// GTL_U
  RcuCE::Service* _t3;		// T3
  RcuCE::Service* _t1_sens;	// T1_SENS
  RcuCE::Service* _t2_sens;	// T2_SENS
  RcuCE::Service* _al_dig_u;	// AL_DIG_U
  RcuCE::Service* _al_ana_u;	// AL_ANA_U
  RcuCE::Service* _t4;		// T4
  RcuCE::Service* _va_rec_up;	// VA_REC_UP
  RcuCE::Service* _va_sup_up;	// VA_SUP_UP
  RcuCE::Service* _va_sup_um;	// VA_SUP_UM
  RcuCE::Service* _va_rec_um;	// VA_REC_UM

  RcuCE::Service* _t1_th;	// T1
  RcuCE::Service* _flash_i_th;	// FLASH_I
  RcuCE::Service* _al_dig_i_th;	// AL_DIG_I
  RcuCE::Service* _al_ana_i_th;	// AL_ANA_I
  RcuCE::Service* _va_rec_ip_th;// VA_REC_IP
  RcuCE::Service* _t2_th;	// T2
  RcuCE::Service* _va_sup_ip_th;// VA_SUP_IP
  RcuCE::Service* _va_rec_im_th;// VA_REC_IM
  RcuCE::Service* _va_sup_im_th;// VA_SUP_IM
  RcuCE::Service* _gtl_u_th;	// GTL_U
  RcuCE::Service* _t3_th;	// T3
  RcuCE::Service* _t1_sens_th;	// T1_SENS
  RcuCE::Service* _t2_sens_th;	// T2_SENS
  RcuCE::Service* _al_dig_u_th;	// AL_DIG_U
  RcuCE::Service* _al_ana_u_th;	// AL_ANA_U
  RcuCE::Service* _t4_th;	// T4
  RcuCE::Service* _va_rec_up_th;// VA_REC_UP
  RcuCE::Service* _va_sup_up_th;// VA_SUP_UP
  RcuCE::Service* _va_sup_um_th;// VA_SUP_UM
  RcuCE::Service* _va_rec_um_th;// VA_REC_UM

  /** Time of construction */
  unsigned int   _t_ctor;
};
  

//____________________________________________________________________
struct FecFactory : public RcuCE::FecFactory 
{
  FecFactory(FeeServer::Main& m, RcuCE::Rcu& r);
  RcuCE::Fec* MakeFec(unsigned char addr);
  /** Get the version string of this library */
  static const std::string& Version();
  /** Get the package name of this library */
  static const std::string& Package();
    
};

#endif
//
// EOF
//
