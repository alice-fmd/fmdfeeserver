#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_scmd.hh>
#include <feeserverxx/fee_dbg.hh>
#include <rcuce/rcuce_ce.hh>
#include <rcuce/rcuce_rcu.hh>
#include <rcuce/rcuce_dcsc.hh>
#include <fmdfeeserver/fec.hh>
#include <rcuce/rcuce_sig.hh>
#include "option.hh"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

//____________________________________________________________________
int 
main(int argc, char** argv) 
{
  command_line cl(argv[0]);
  option<std::string>  opt_n('n', "Set name of server", "FMD-FEE_0_0_0","NAME");
  option<std::string>  opt_D('D', "Set name of detector", "FMD", "DETECTOR");
  option<std::string>  opt_N('N', "Set DIM DNS host",      "localhost","HOST");
  option<std::string>  opt_M('M', "Set emulation memory dump",      "","FILE");
  option<bool>         opt_e('e', "Enable emulation mode",       false);
  option<bool>         opt_d('d', "Enable debug output",         false);
  option<bool>         opt_v('v', "Enable verbose output",       false);
  option<unsigned int> opt_t('t', "Enable trace output",         0, "LEVEL");
  option<bool>         opt_i('i', "Enable informative output",   false);
  option<bool>         opt_f('f', "Enable implicit flicker",     false);
  option<bool>         opt_c('c', "Monitor trigger configuration",     false);
  option<bool>         opt_r('r', "Monitor TTC run state",     false);
  option<bool>         opt_H('H', "Monitor HITLIST",     false);
  option<bool>         opt_S('S', "Monitor Status memory",     false);
  option<bool>         opt_C('C', "Monitor temperatures on RCU",     false);
  option<bool>         opt_G('G', "Enable glitch filtering",     false);
  option<unsigned int> opt_w('w', "Wait between updates",         2000,"MSECS");
  option<unsigned int> opt_T('T', "Watchdog timeout",            20000,"MSECS");
  option<unsigned int> opt_V('V', "Emulated Firmware version",0x080808,"VERS");
  option<bool>         opt_A('A', "Also serve thresholds", true);
  option<unsigned int> opt_W('W', "Delay before updating 1st time", 0, "SECS");
  option<unsigned int> opt_s('s', "Explicit sleep for dcsc dummy", 1, "MSECS");
  option<std::string>  opt_L('L', "Set debug log file",      "","FILE");
  cl.add_option(opt_n);
  cl.add_option(opt_D);
  cl.add_option(opt_N);
  cl.add_option(opt_M);
  cl.add_option(opt_e);
  cl.add_option(opt_d);
  cl.add_option(opt_v);
  cl.add_option(opt_t);
  cl.add_option(opt_i);
  cl.add_option(opt_f);
  cl.add_option(opt_w);
  cl.add_option(opt_c);
  cl.add_option(opt_r);
  cl.add_option(opt_S);
  cl.add_option(opt_C);
  cl.add_option(opt_G);
  cl.add_option(opt_H);
  cl.add_option(opt_T);
  cl.add_option(opt_V);
  cl.add_option(opt_A);
  cl.add_option(opt_W);
  cl.add_option(opt_s);
  cl.add_option(opt_L);
  if (!cl.handle(argc, argv)) return 1;
  if (cl.got_help()) return 0;
  
  std::string  name     = opt_n;
  std::string  detector = opt_D;
  std::string  dns      = opt_N;
  std::string  dump     = opt_M;
  bool         emul     = opt_e;
  bool         debug    = opt_d;
  bool         verbose  = opt_v;
  bool         info     = opt_i;
  bool         flicker  = opt_f;
  bool         montrg   = opt_c;
  bool         monttc   = opt_r;
  bool         monhit   = opt_H;
  bool         monstm   = opt_S;
  bool         montmp   = opt_C;
  unsigned int wait     = opt_w;// In milliseconds
  unsigned int wdto     = opt_T; // in milliseconds
  unsigned int fwvers   = opt_V;
  unsigned int trace    = opt_t;
  unsigned int dsleep   = opt_s;
  std::string  log_file = opt_L;
  
  Fec::_also_thresholds  = opt_A;
  Fec::_delay            = opt_W;
  Fec::_glitch_filtering = opt_G;

  install_signal_handler();
  DTRACE(trace);
  //EndOptions

  std::cout << "Creating feeserver" << std::endl;
  FeeServer::Main m(name, detector, dns);
  unsigned int mask = (FeeServer::Message::Warning | 
		       FeeServer::Message::Error | 
		       FeeServer::Message::AuditFailure | 
		       FeeServer::Message::AuditSuccess |
		       FeeServer::Message::Alarm);
  // m.GetLogLevel();
  if (debug) mask |= FeeServer::Message::Debug;
  if (info)  mask |= FeeServer::Message::Info;
  m.SetLogLevel(mask);
  m.SetWatchdogTimeout(wdto);

  // m.AddCommand(new UpdateServer(m));
  m.AddCommand(new FeeServer::RestartServer(m));
  // m.AddCommand(new FeeServer::RebootMachine(m));
  // m.AddCommand(new FeeServer::ShutdownMachine(m));
  m.AddCommand(new FeeServer::ExitServer(m));
  m.AddCommand(new FeeServer::SetDeadband(m));
  m.AddCommand(new FeeServer::GetDeadband(m));
  m.AddCommand(new FeeServer::SetIssueTimeout(m));
  m.AddCommand(new FeeServer::GetIssueTimeout(m));
  m.AddCommand(new FeeServer::SetUpdateRate(m));
  m.AddCommand(new FeeServer::GetUpdateRate(m));
  m.AddCommand(new FeeServer::SetLogLevel(m));
  m.AddCommand(new FeeServer::GetLogLevel(m));
  m.AddCommand(new FeeServer::ShowServices(m));
  //EndFee

  RcuCE::ControlEngine ce(m, wait);
  m.SetCtrlEngine(ce);
  
  RcuCE::Dcsc* dcsc = 0;
  if (emul) dcsc = new RcuCE::DcscDummy(fwvers, dump, dsleep);
  else      dcsc = new RcuCE::Dcsc("",verbose);

  RcuCE::Rcu rcu(*dcsc, m);
  FecFactory f(m, rcu);
  rcu.SetFecFactory(f);
  rcu.EnableAFLFlicker(flicker);
  if (monhit) rcu.MonitorHitList();
  if (montrg) rcu.MonitorTrigConf();
  if (monttc) rcu.MonitorRunState();
  if (monstm) rcu.MonitorStatusMemory();
  if (montmp) rcu.MonitorTemps();
  ce.AddProvider(rcu);
  ce.AddHandler(rcu);
  ce.AddHandler(*dcsc);
  ce.AddFsm(rcu);

  std::ofstream* dlog = 0;
  if (!log_file.empty()) { 
    dlog = new std::ofstream(log_file.c_str());
    *dlog << "# Trace of monitored values.\n"
	  << "# Line added only on change\n"
	  << "# Format of lines is\n"
	  << "# \n"
	  << "#   TIME,FEC,ADDR,NAME,VALUE,QUALITY\n"
	  << "# \n" 
	  << "# FEC=0xFFF means RCU" << std::endl;
  }
  RcuCE::Fec::SetLog(dlog);
  RcuCE::RcuService::SetLogFile(dlog);
  //EndRCU
  
  std::cout << "Running fee server\n\n"
	    << "   Core:                  " << m.PackageString() << " " 
	    << m.VersionString() << "\n"
	    << "   CE:                    " << ce.PackageString() << " " 
	    << ce.VersionString() << "\n"
	    << "   This:                  " << PACKAGE_NAME << " " 
	    << PACKAGE_VERSION << "-" << RELEASE "\n\n"
	    << "   Name:                  " << name     << "\n"
	    << "   Detector:              " << detector << "\n"
	    << "   DIM DNS:               " << dns      << "\n"
	    << "   Emulation memory file: " << dump     << "\n"
	    << "   Emulation:             " << emul     << "\n"
	    << "   Debug enabled:         " << debug    << "\n"
	    << "   Verbose output:        " << verbose  << "\n"
	    << "   Informative messages   " << info     << "\n"
	    << "   Implicit flick CARDSW: " << flicker  << "\n"
	    << "   Seconds to wait:       " << wait     << "\n"
	    << "   Watchdog timeout:      " << wdto     << "\n"
	    << "   Firmware version:      " << fwvers   << "\n"
	    << "   Enable call trace:     " << trace    << "\n"
	    << "   Monitor trigconf:      " << montrg   << "\n"
	    << "   Monitor ttc run state: " << monttc   << "\n"
	    << "   Monitor hitlist:       " << monhit   << "\n"
	    << "   Monitor status memory: " << monstm   << "\n"
	    << "   Monitor RCU temps:     " << montmp   << "\n"
	    << "   Serve thresholds:      " << Fec::_also_thresholds << "\n"
	    << "   1st time delay:        " << Fec::_delay << "\n"
	    << "   Debug log file:        " << log_file << "\n"
	    << std::endl;
  

  int ret = m.Run();
  if (dlog) dlog->close();
  return ret;
}
//
// EOF
//
