dnl -*- mode: autoconf -*- 
dnl
dnl $Id: configure.ac,v 1.10 2012-03-07 11:29:48 cholm Exp $
dnl template for the configuration script for the FeeServer
dnl with RCU-like ControlEngines
dnl 
dnl ------------------------------------------------------------------
AC_INIT([FMD FeeServer] , [1.1], [cholm@nbi.dk], fmdfeeserver)
AC_CONFIG_AUX_DIR(config)
dnl AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_SRCDIR(fmdfeeserver/server.cc)
AC_CANONICAL_SYSTEM
AC_PREFIX_DEFAULT(${PWD})
AM_INIT_AUTOMAKE
AC_PROG_CC
AC_PROG_CXX
AC_PROG_LIBTOOL

AC_DEBUG
AC_OPTIMIZATION

sovers=1
AC_SUBST(sovers)
RELEASE=1
AC_SUBST(RELEASE)

dnl the ARM_COMPILING switch is used in the feeserver/Makefile.am
dnl to set the linkink option to libtool (-all-static)
dnl so far I couldn't find out how to do this on the configure 
dnl command line. Although this flag should be set according to
dnl users choice of --anable/disable-shared
if test "x$cross_compiling" = "xyes" ; then
 case "${host}" in
 arm*)arm_compiling=yes ;;
 *)   arm_compiling=no ;; 
 esac
fi
AC_MSG_CHECKING(whether we're compiling for arm-linux)
AM_CONDITIONAL(ARM_COMPILING, test x$arm_compiling = xyes )
AC_MSG_RESULT($arm_compiling)


dnl ------------------------------------------------------------------
dnl thread flags for the feeserver
AC_THREAD_FLAGS

dnl ------------------------------------------------------------------
dnl Check for DIM
AC_ROOT_DIM([19.11],[],[AC_MSG_WARN([I may need DIM])])
#if test "x$arm_compiling" = "xyes" ; then 
#  AC_DIM([LIBS="$LIBS -ldim"],[AC_MSG_WARN([I may need DIM])])
#fi

dnl ------------------------------------------------------------------
dnl Check for FeeServer++ library
AC_FEESERVERXX([1.2],[],[AC_MSG_ERROR(I need libfeeserverxx)])

dnl ------------------------------------------------------------------
dnl Check for DCSC library
AC_DCSC([0.5],[],[AC_MSG_ERROR(I need libdcsc)])

dnl ------------------------------------------------------------------
dnl Check for DCSC library
AC_RCUCE([1.1],[],[AC_MSG_ERROR(I need librcuce)])

dnl ------------------------------------------------------------------
dnl
dnl Documentation
dnl
AC_ARG_VAR(DOXYGEN, The Documentation Generator)
AC_PATH_PROG(PERL, perl)
AC_PATH_PROG(DOXYGEN, doxygen)
AC_ARG_ENABLE(docs,
  [AC_HELP_STRING([--enable-docs],
      [Turn on generation of documentation])],
  [],[enable_docs=no])
if test "x$enable_docs" != "xyes" ; then DOXYGEN= ; fi
AC_MSG_CHECKING(whether we should do the documentation)
AC_MSG_RESULT($enable_docs)
AM_CONDITIONAL(HAVE_DOXYGEN, test ! "x$DOXYGEN" = "x")
HAVE_DOT=NO
DOT_PATH=
AC_PATH_PROG(DOT, dot)
if ! test "x$DOT" = "x" ; then
   HAVE_DOT=YES
   DOT_PATH=`dirname $DOT`
fi
AC_SUBST([HAVE_DOT])
AC_SUBST([DOT_PATH])

dnl ------------------------------------------------------------------
dnl
dnl RPM stuff 
dnl
RPM_VERSION=`echo $VERSION | tr '-' '_'`
AC_SUBST(RPM_VERSION)
AC_SUBST(RPM_PREFIX)
AC_SUBST(RPM_BUILD_ARGS)

dnl ------------------------------------------------------------------
AC_CONFIG_FILES([Makefile 
		 fmdfeeserver/Makefile
		 support/fmdfeeserver.spec
		 support/Makefile
		 support/start_fmdfeeserver.sh
		 support/S90fmdfeeserver
		 doc/Makefile
		 doc/doxyconfig
		 debian/Makefile])

AC_OUTPUT
dnl
dnl EOF
dnl

