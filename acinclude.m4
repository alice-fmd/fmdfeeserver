dnl
dnl $Id: acinclude.m4,v 1.5 2012-03-07 11:29:48 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_THREAD_FLAGS],
[
  dnl ------------------------------------------------------------------
  dnl Thread flags to use 
  case $host_os:$host_cpu in
  solaris*|sun*)	  CFLAGS="$CFLAGS -mt"
			  LIBS="$LIBS -lposix4"		;;
  hp-ux*|osf*|aix*)					;;
  # The DIM libary should not be threaded on an ARM chip, but the
  # FeeServer  assumes that it is so we take the next line out and
  # default to normal Linux
  # linux*:arm*)					;;
  linux*)		  LIBS="$LIBS -pthread"		;;
  lynxos*:rs6000)	  CFLAGS="$CFLAGS -mthreads"	;;
  *)			  LIBS="$LIBS -lpthread"	;;
  esac
])

AC_DEFUN([AC_DIM_ARCH],
[
  AC_REQUIRE([AC_PROG_CC])
  dnl ------------------------------------------------------------------
  dnl Byte order 
  AH_TEMPLATE(MIPSEB, [Big-endian machine])
  AH_TEMPLATE(MIPSEL, [Little-endian machine])
  AC_C_BIGENDIAN([AC_DEFINE([MIPSEB])],[AC_DEFINE([MIPSEL])])
  AC_DEFINE([PROTOCOL],[1])

  AC_REQUIRE([AC_THREAD_FLAGS])
  dnl ------------------------------------------------------------------
  dnl Misc flags per host OS/CPU
  case $host_os:$host_cpu in
  sun*)		  AC_DEFINE([sunos])		;;
  solaris*)	  AC_DEFINE([solaris])	
		  LIBS="$LIBS -lsocket -lnsl"	;;
  hp-ux*)	  AC_DEFINE([hpux])		;;
  osf*)		  AC_DEFINE([osf])		;;
  aix*)		  AC_DEFINE([aix])	
		  AC_DEFINE([unix])
		  AC_DEFINE([_BSD])		;;
  lynxos*:rs6000) AC_DEFINE([LYNXOS])	
		  AC_DEFINE([RAID])
		  AC_DEFINE([unix])		
		  CPPFLAGS="$CPPFLAGS -I/usr/include/bsd -I/usr/include/posix"
		  LDFLAGS="$LDFLAGS -L/usr/posix/usr/lib"
		  LIBS="$LIBS -lbsd"		;;
  lynxos*:*86*)	  AC_DEFINE([LYNXOS])	
		  AC_DEFINE([unix])		
		  LIBS="$LIBS -lbsd -llynx"	;;
  lynxos*)	  AC_DEFINE([LYNXOS])	
		  AC_DEFINE([unix])		
		  LIBS="$LIBS -lbsd"		;;
  linux*)	  AC_DEFINE([linux])		
		  AC_DEFINE([unix])		;;
  esac
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_DIM],
[
  AC_REQUIRE([AC_DIM_ARCH])
  dnl ------------------------------------------------------------------
  dnl AC_ARG_ENABLE([builtin-dim],
  dnl 	        [AC_HELP_STRING([--enable-builtin-dim],
  dnl 	                        [Use shipped DIM, not systems])])
  have_dim=no
  AC_LANG_PUSH(C++)
  AC_CHECK_LIB(dim, [dic_get_server],
	       [AC_CHECK_HEADERS([dim/dim.hxx],[have_dim=yes])])
  AC_LANG_POP(C++)
  dnl if test "x$enable_builtin_dim" = "xyes" ; then 
  dnl    have_dim=no
  dnl fi
  dnl AC_MSG_CHECKING(whether to use possible system DIM installed)
  dnl if test "x$have_dim" = "xno" ; then 
  dnl    AC_CONFIG_SUBDIRS(dim)
  dnl fi
  dnl AC_MSG_RESULT($have_dim)
  dnl AM_CONDITIONAL(NEED_DIM, test "x$have_dim" = "xno")

  if test "x$have_dim" = "xyes" ; then 
    ifelse([$1], , :, [$1])     
  else 
    ifelse([$2], , :, [$2])     
  fi
])

dnl ------------------------------------------------------------------
dnl -*- mode: Autoconf -*- 
dnl
dnl AC_ROOT_DIM([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_DIM],
[
    AC_ARG_WITH([dim-prefix],
        [AC_HELP_STRING([--with-dim-prefix],
		[Prefix where Dim is installed])],
        dim_prefix=$withval, dim_prefix="")

    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    AC_PATH_PROG(DIM_CONFIG, dim-config, no)
    dim_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Dim version >= $dim_min_version)

    dim_found=no    
    if test "x$DIM_CONFIG" != "xno" ; then 
       DIM_CFLAGS=`$DIM_CONFIG --cflags`
       DIM_CPPFLAGS=`$DIM_CONFIG --cppflags`
       DIM_INCLUDEDIR=`$DIM_CONFIG --includedir`
       DIM_LIBS=`$DIM_CONFIG --libs`
       DIM_LIBDIR=`$DIM_CONFIG --libdir`
       DIM_LDFLAGS=`$DIM_CONFIG --ldflags`
       DIM_PREFIX=`$DIM_CONFIG --prefix`
       DIM_JNILIBS=`$DIM_CONFIG --libs`
       
       dim_version=`$DIM_CONFIG -V` 
       dim_vers=`echo $dim_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       dim_regu=`echo $dim_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $dim_vers -ge $dim_regu ; then 
            dim_found=yes
       fi
    fi
    AC_MSG_RESULT($dim_found - is $dim_version) 

    if test "x$dim_found" = "xyes" ; then 
       save_LDFLAGS=$LDFLAGS
       save_LIBS=$LIBS
       save_CPPFLAGS=$CPPFLAGS 

       LDFLAGS="$LDFLAGS $DIM_LDFLAGS"
       LIBS="$LIBS $DIM_LIBS"
       CPPFLAGS="$CPPFLAGS $DIM_CPPFLAGS" 

       # Change language 
       AC_LANG_PUSH(C++)

       have_dim_dim_hxx=0
       AC_CHECK_HEADERS([dim/dim.hxx],[have_dim_dim_hxx=1])
       
       # Check libraru 
       have_libdim=0
       AC_MSG_CHECKING([for -ldim])
       AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <dim/dim.hxx>],
       				       [DimTimer timer])],
		      [have_libdim=1])
		     
       if test $have_dim_dim_hxx -gt 0 && test $have_libdim -gt 0 ; then 
           AC_DEFINE(HAVE_DIM)
       else
           dim_found=no
       fi
       AC_MSG_RESULT($dim_found)

       # Restore 
       AC_LANG_POP(C++)
       CPPFLAGS=$save_CPPFLAGS
       LDFLAGS=$save_LDFLAGS
       LIBS=$save_LIBS
    fi	

    if test "x$dim_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(DIM_PREFIX)
    AC_SUBST(DIM_CFLAGS)
    AC_SUBST(DIM_CPPFLAGS)
    AC_SUBST(DIM_INCLUDEDIR)
    AC_SUBST(DIM_LDFLAGS)
    AC_SUBST(DIM_LIBDIR)
    AC_SUBST(DIM_LIBS)
    AC_SUBST(DIM_JNILIBS)
])

dnl
dnl EOF
dnl 

dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])],
    [],[enable_debug=yes])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    AC_DEFINE(__DEBUG)
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])],
    [],[enable_optimization=yes])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
dnl -*- mode: Autoconf -*- 
dnl
dnl  Generic feeserverxx framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_FEESERVER([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_FEESERVERXX],
[
    AC_REQUIRE([AC_THREAD_FLAGS])
    AC_REQUIRE([AC_ROOT_DIM])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([feeserverxx],
        [AC_HELP_STRING([--with-feeserverxx],	
                        [Prefix where FeeServer++ is installed])],
	[],[with_feeserverxx="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([feeserverxx-url],
        [AC_HELP_STRING([--with-feeserverxx-url],
		[Base URL where the FeeServer++ dodumentation is installed])],
        feeserverxx_url=$withval, feeserverxx_url="")
    if test "x${FEESERVERXX_CONFIG+set}" != xset ; then 
        if test "x$with_feeserverxx" != "xno" ; then 
	    FEESERVERXX_CONFIG=$with_feeserverxx/bin/feeserverxx-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_feeserverxx" != "xno" ; then 
        AC_PATH_PROG(FEESERVERXX_CONFIG, feeserverxx-config, no)
        feeserverxx_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for FeeServer++ version >= $feeserverxx_min_version)

        # Check if we got the script
        with_feeserverxx=no    
        if test "x$FEESERVERXX_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           FEESERVERXX_CPPFLAGS=`$FEESERVERXX_CONFIG --cppflags`
           FEESERVERXX_INCLUDEDIR=`$FEESERVERXX_CONFIG --includedir`
           FEESERVERXX_LIBS=`$FEESERVERXX_CONFIG --libs`
           FEESERVERXX_LTLIBS=`$FEESERVERXX_CONFIG --ltlibs`
           FEESERVERXX_LIBDIR=`$FEESERVERXX_CONFIG --libdir`
           FEESERVERXX_LDFLAGS=`$FEESERVERXX_CONFIG --ldflags`
           FEESERVERXX_LTLDFLAGS=`$FEESERVERXX_CONFIG --ltldflags`
           FEESERVERXX_PREFIX=`$FEESERVERXX_CONFIG --prefix`
           
           # Check the version number is OK.
           feeserverxx_version=`$FEESERVERXX_CONFIG -V` 
           feeserverxx_vers=`echo $feeserverxx_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           feeserverxx_regu=`echo $feeserverxx_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $feeserverxx_vers -ge $feeserverxx_regu ; then 
                with_feeserverxx=yes
           fi
        fi
        AC_MSG_RESULT($with_feeserverxx - is $feeserverxx_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_FEESERVERXX, [Whether we have feeserverxx])
    
    
        if test "x$with_feeserverxx" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
	    save_LIBS=$LIBS
            LDFLAGS="$LDFLAGS $DIM_LDFLAGS $FEESERVERXX_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $DIM_CPPFLAGS $FEESERVERXX_CPPFLAGS"
	    LIBS="$FEESERVERXX_LIBS $DIM_LIBS $LIBS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_feeserverxx_fee_main_hh=0
            AC_CHECK_HEADER([feeserverxx/fee_main.hh], 
                            [have_feeserverxx_fee_main_hh=1])
    
            # Check the library. 
            have_libfeeserverxx=no
            AC_MSG_CHECKING(for -lfeeserverxx)
            AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <feeserverxx/fee_main.hh>],
                                           [FeeServer::Main m("a","b","c");])], 
                                            [have_libfeeserverxx=yes])
            AC_MSG_RESULT($have_libfeeserverxx)
    
            if test $have_feeserverxx_fee_main_hh -gt 0    && \
                test "x$have_libfeeserverxx"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_FEESERVERXX)
            else 
                with_feeserverxx=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
    	CPPFLAGS=$save_CPPFLAGS
    	LDFLAGS=$save_LDFLAGS
	LIBS=$save_LIBS
        fi
    
        AC_MSG_CHECKING(where the FeeServer++ documentation is installed)
        if test "x$feeserverxx_url" = "x" && \
    	test ! "x$FEESERVERXX_PREFIX" = "x" ; then 
           FEESERVERXX_URL=${FEESERVERXX_PREFIX}/share/doc/feeserverxx
        else 
    	   FEESERVERXX_URL=$feeserverxx_url
        fi	
        AC_MSG_RESULT($FEESERVERXX_URL)
    fi
   
    if test "x$with_feeserverxx" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(FEESERVERXX_URL)
    AC_SUBST(FEESERVERXX_PREFIX)
    AC_SUBST(FEESERVERXX_CPPFLAGS)
    AC_SUBST(FEESERVERXX_INCLUDEDIR)
    AC_SUBST(FEESERVERXX_LDFLAGS)
    AC_SUBST(FEESERVERXX_LIBDIR)
    AC_SUBST(FEESERVERXX_LIBS)
    AC_SUBST(FEESERVERXX_LTLIBS)
    AC_SUBST(FEESERVERXX_LTLDFLAGS)
])
dnl
dnl EOF
dnl 

dnl ------------------------------------------------------------------
dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.5 2012-03-07 11:29:48 cholm Exp $ 
dnl  
dnl  ROOT generic dcsc framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_DCSC([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_DCSC],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([dcsc],
        [AC_HELP_STRING([--with-dcsc],	
                        [Prefix where Dcsc is installed])],
	[],[with_dcsc="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([dcsc-url],
        [AC_HELP_STRING([--with-dcsc-url],
		[Base URL where the Dcsc dodumentation is installed])],
        dcsc_url=$withval, dcsc_url="")
    if test "x${DCSC_CONFIG+set}" != xset ; then 
        if test "x$with_dcsc" != "xno" ; then 
	    DCSC_CONFIG=$with_dcsc/bin/dcsc-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_dcsc" != "xno" ; then 
        AC_PATH_PROG(DCSC_CONFIG, dcsc-config, no)
        dcsc_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for Dcsc version >= $dcsc_min_version)

        # Check if we got the script
        with_dcsc=no    
        if test "x$DCSC_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           DCSC_CPPFLAGS=`$DCSC_CONFIG --cppflags`
           DCSC_INCLUDEDIR=`$DCSC_CONFIG --includedir`
           DCSC_LIBS=`$DCSC_CONFIG --libs`
           DCSC_LTLIBS=`$DCSC_CONFIG --ltlibs`
           DCSC_LIBDIR=`$DCSC_CONFIG --libdir`
           DCSC_LDFLAGS=`$DCSC_CONFIG --ldflags`
           DCSC_LTLDFLAGS=`$DCSC_CONFIG --ltldflags`
           DCSC_PREFIX=`$DCSC_CONFIG --prefix`
           
           # Check the version number is OK.
           dcsc_version=`$DCSC_CONFIG -V` 
           dcsc_vers=`echo $dcsc_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           dcsc_regu=`echo $dcsc_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $dcsc_vers -ge $dcsc_regu ; then 
                with_dcsc=yes
           fi
        fi
        AC_MSG_RESULT($with_dcsc - is $dcsc_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_DCSC, [Whether we have dcsc])
    
    
        if test "x$with_dcsc" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
	    save_LIBS=$LIBS
            LDFLAGS="$LDFLAGS $DCSC_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $DCSC_CPPFLAGS"
     
     	    # Check for a header 
            have_dcsc_dcscMsgBufferInterface_h=0
            AC_CHECK_HEADER([dcsc/dcscMsgBufferInterface.h], 
                            [have_dcsc_dcscMsgBufferInterface_h=1])
    
            # Check the library. 
            have_libdcsc=no
            AC_MSG_CHECKING(for -ldcsc)
            AC_CHECK_LIB([dcsc],[initRcuAccess],[have_libdcsc=yes])
            AC_MSG_RESULT($have_libdcsc)
    
            if test $have_dcsc_dcscMsgBufferInterface_h -gt 0    && \
                test "x$have_libdcsc"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_DCSC)
            else 
                with_dcsc=no
            fi
    	    CPPFLAGS=$save_CPPFLAGS
    	    LDFLAGS=$save_LDFLAGS
	    LIBS=$save_LIBS
        fi
    
        AC_MSG_CHECKING(where the Dcsc documentation is installed)
        if test "x$dcsc_url" = "x" && \
    	test ! "x$DCSC_PREFIX" = "x" ; then 
           DCSC_URL=${DCSC_PREFIX}/share/doc/dcsc
        else 
    	   DCSC_URL=$dcsc_url
        fi	
        AC_MSG_RESULT($DCSC_URL)
    fi
   
    if test "x$with_dcsc" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(DCSC_URL)
    AC_SUBST(DCSC_PREFIX)
    AC_SUBST(DCSC_CPPFLAGS)
    AC_SUBST(DCSC_INCLUDEDIR)
    AC_SUBST(DCSC_LDFLAGS)
    AC_SUBST(DCSC_LIBDIR)
    AC_SUBST(DCSC_LIBS)
    AC_SUBST(DCSC_LTLIBS)
    AC_SUBST(DCSC_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.5 2012-03-07 11:29:48 cholm Exp $ 
dnl  
dnl  ROOT generic rcuce framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_RCUCE([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUCE],
[
    AC_REQUIRE([AC_FEESERVERXX])
    AC_REQUIRE([AC_DCSC])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcuce],
        [AC_HELP_STRING([--with-rcuce],	
                        [Prefix where RcuCE is installed])],
	[],[with_rcuce="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcuce-url],
        [AC_HELP_STRING([--with-rcuce-url],
		[Base URL where the RcuCE dodumentation is installed])],
        rcuce_url=$withval, rcuce_url="")
    if test "x${RCUCE_CONFIG+set}" != xset ; then 
        if test "x$with_rcuce" != "xno" ; then 
	    RCUCE_CONFIG=$with_rcuce/bin/rcuce-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_rcuce" != "xno" ; then 
        AC_PATH_PROG(RCUCE_CONFIG, rcuce-config, no)
        rcuce_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for RcuCE version >= $rcuce_min_version)

        # Check if we got the script
        with_rcuce=no    
        if test "x$RCUCE_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           RCUCE_CPPFLAGS=`$RCUCE_CONFIG --cppflags`
           RCUCE_INCLUDEDIR=`$RCUCE_CONFIG --includedir`
           RCUCE_LIBS=`$RCUCE_CONFIG --libs`
           RCUCE_LTLIBS=`$RCUCE_CONFIG --ltlibs`
           RCUCE_LIBDIR=`$RCUCE_CONFIG --libdir`
           RCUCE_LDFLAGS=`$RCUCE_CONFIG --ldflags`
           RCUCE_LTLDFLAGS=`$RCUCE_CONFIG --ltldflags`
           RCUCE_PREFIX=`$RCUCE_CONFIG --prefix`
           
           # Check the version number is OK.
           rcuce_version=`$RCUCE_CONFIG -V` 
           rcuce_vers=`echo $rcuce_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           rcuce_regu=`echo $rcuce_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $rcuce_vers -ge $rcuce_regu ; then 
                with_rcuce=yes
           fi
        fi
        AC_MSG_RESULT($with_rcuce - is $rcuce_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_RCUCE, [Whether we have rcuce])
    
    
        if test "x$with_rcuce" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
	    save_LIBS=$LIBS
            LDFLAGS="$LDFLAGS $FEESERVER_LDFLAGS $DCSC_LDFLAGS $RCUCE_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $FEESERVER_CPPFLAGS $DCSC_CPPFLAGS $RCUCE_CPPFLAGS"
     	    LIBS="$LIBS $RCUCE_LIBS $FEESERVER_LIBS $DCSC_LIBS"
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_rcuce_rcuce_ce_hh=0
            AC_CHECK_HEADER([rcuce/rcuce_ce.hh], 
                            [have_rcuce_rcuce_ce_hh=1])
    
            # Check the library. 
            have_librcuce=no
            AC_MSG_CHECKING(for -lrcuce)
            AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <rcuce/rcuce_dcsc.hh>],
                                            [RcuCE::Dcsc d;])], 
                                            [have_librcuce=yes])
            AC_MSG_RESULT($have_librcuce)
    
            if test $have_rcuce_rcuce_ce_hh -gt 0    && \
                test "x$have_librcuce"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_RCUCE)
            else 
                with_rcuce=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
    	    CPPFLAGS=$save_CPPFLAGS
      	    LDFLAGS=$save_LDFLAGS
      	    LIBS=$save_LIBS
        fi
    
        AC_MSG_CHECKING(where the RcuCE documentation is installed)
        if test "x$rcuce_url" = "x" && \
    	test ! "x$RCUCE_PREFIX" = "x" ; then 
           RCUCE_URL=${RCUCE_PREFIX}/share/doc/rcuce
        else 
    	   RCUCE_URL=$rcuce_url
        fi	
        AC_MSG_RESULT($RCUCE_URL)
    fi
   
    if test "x$with_rcuce" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUCE_URL)
    AC_SUBST(RCUCE_PREFIX)
    AC_SUBST(RCUCE_CPPFLAGS)
    AC_SUBST(RCUCE_INCLUDEDIR)
    AC_SUBST(RCUCE_LDFLAGS)
    AC_SUBST(RCUCE_LIBDIR)
    AC_SUBST(RCUCE_LIBS)
    AC_SUBST(RCUCE_LTLIBS)
    AC_SUBST(RCUCE_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
