#!/bin/sh

start_three()
{
    for i in 1 2 3 ; do 
	start_one $i 
    done
}

start_one()
{
    i=$1
    ./fmdfeeserver/fmdfeeserver 	\
	-N localhost 			\
	-n FMD-FEE_0_0_$i 		\
	-e 				\
	-V 0x101010			\
  	-c 				\
	-i				\
	-L foo.log			\
	-C				\
	-d 				\
	-t 6				\
	> FMD-FEE_0_0_$i.log 2>&1 & 
}

stop_them()
{
    killall -9 fmdfeeserver 2>/dev/null
}

get_state() 
{
    i=$1
    s="FMD-FEE_0_0_${i}_RCU_STATE"
    n=`DIM_DNS_NODE=localhost dim_get_service $s `
    case "$n" in 
	*FATAL*)     return ;;
	*Available*) return ;;
	*) 
	    j=`echo $n | sed -e "s/Service $s Contents : .* D \([0-9]*\)/\1/"`
	    ;; 
    esac
    case $j in
	1) t="idle" ;; 
	2) t="standby" ;; 
	3) t="downloading" ;; 
	4) t="ready" ;; 
	5) t="mixed" ;;
	6) t="error" ;; 
	7) t="running" ;; 
	*) t="unknown"  ;;
    esac
    echo "FMD-FEE_0_0_${i} in state $t" 
}

case $1 in 
    start|restart)
	stop_them
	start_one 0
	;;
    stop)
	stop_them
	;;
    status) 
	get_state 0
	;;
    *)
	echo "Usage: $0 [start|stop|restart|status]"
	exit 1
	;;
esac
